import { Navigate } from "react-router-dom";
import { useContext } from "react"
import Banner from "../components/Banner"
import UserContext from "../UserContext";
import Highlights from "../components/Highlights"
import Products from "./Products"
import HotDeals from "../components/HotDeals"



export default function Home(){

	const { user } = useContext(UserContext);

		const data = {
	title: "Online Computer Store",
	content: "Pro is better than geeks.",
	destination: "/login",
	label: "Buy Now!"
	}





	return(

		<>{
			(user.isAdmin)
			?
			<Navigate to="/dashboard" />
			: 

		<div>
		<Banner bannertop={data}/>
		<div className="py-5">
		<Highlights />
		</div>
		<HotDeals/>
		
		<Products/>
		</div>
		}
		
		</>
		
		)
}

