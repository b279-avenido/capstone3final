import { Card, Button } from 'react-bootstrap';
import { useState } from "react"
import {Link} from "react-router-dom"



export default function ProductCard({productProp}){

     console.log(productProp.name);
  
    console.log(typeof productProp);

    const { _id, name, description, price, stocks} = productProp;


    return(
        <Card className="my-3">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Card.Subtitle>Stocks:</Card.Subtitle>
                <Card.Text>{stocks}</Card.Text>
                <Link className="btn btn-success" to={`/productView/${_id}`}>Details</Link>
            </Card.Body>
        </Card>

        )
}