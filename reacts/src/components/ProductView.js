import{useState, useEffect, useContext} from "react";
import { Container, Card, Button, Row, Col, Form } from "react-bootstrap"
import { useParams, useNavigate, Link, Navigate } from "react-router-dom"
import UserContext from "../UserContext"
import Swal from "sweetalert2"


export default function ProductView(){

	const data = {
		title: "YOU ARE ABOUT TO MAKE A TRANSACTION",
		content: "Buy products using your E-wallet. Less hassle, more efficient!",
		destination: "/products",
		label: "Other Products",
		
	}

	const { user } = useContext(UserContext);

	
	const { productId } = useParams("");
	const navigate = useNavigate();

	const [name, setName] = useState("");
	// const [productName, setProductName] = useState('');
	const [description, setDescription] = useState("");
	const [quantity, setQuantity] = useState(1); // new state variable for quantity
	const [isActive, setIsActive] = useState(false);
	const [price, setPrice] = useState(0);
	

	useEffect(() => {
		console.log(productId);
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			// setProductName(data.productName)
			setDescription(data.description);
			setPrice(data.price)
		})
	}, [productId])


	const makeAnOrder = (e) => {
        e.preventDefault();
    
        fetch(`http://localhost:4000/order/${productId}/addToOrder`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                quantity: quantity
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
    
            if (data) {
                Swal.fire({
                    title: "PURCHASED SUCCESSFULLY!",
                    icon: "success",
                    text: `"The new service was added.`,
                });

                 navigate("/products");
    
            }
            else {
                Swal.fire({
                    title: "PURCHASED SUCCESSFULLY!",
                    icon: "error",
                    text: `The system is experiencing trouble at the moment. Please try again later.`,
                });
            }
    
        })
        setQuantity(0);
    }



    useEffect(() => {

		if (quantity > 0) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	
	}, [quantity]);




	
		

	
	return(

		<>
		{(user.isAdmin)
		?
		<Navigate to="/dashboard" />
		:
		<div>
			<Container>
			  <Row>
				<Col>
					<Card>
						<Card.Body>
							<Card.Title>{name}</Card.Title>
							{/*<Card.Title>{productName}</Card.Title>*/}
							<Card.Subtitle>Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>

							<Form className='' onSubmit={e => makeAnOrder(e)}>
                    
                        {/* Name */}
                        <Form.Group className="mb-3" controlId="FirstName">
                        
                        <Form.Control type="number" value={quantity} onChange={e => setQuantity(e.target.value)} placeholder="Enter Quantity" required/>
                        </Form.Group>
                        {/* Submit Button */}

                        { (user.token !== null)? //isActive ?
                        <Button variant="primary" type="submit" id="submitBtn">Buy!</Button>
                        :
                        <Button variant="danger" type="submit" id="submitBtn" disabled>Buy!</Button>
                        }
                        </Form>

														

							
						</Card.Body>
						

					</Card>
				</Col>
			  </Row>
			</Container>
		</div>
		}
		</>
			
		)
}