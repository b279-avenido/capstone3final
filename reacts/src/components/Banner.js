import { Row, Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom"

export default function Banner({bannertop}){
	console.log(bannertop);

	const {title, content, destination, label} = bannertop;

	return(
			<Row>
				<Col className="p-5">
					<h1>{title}</h1>
					<h5 className="pt-4 pb-2">{content}</h5>
					<Button as={Link} to={destination} className="btn-danger" variant="">{label}</Button>
				</Col>
			</Row>
		)
}